package engine.data;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

public interface IJumpMemory extends IAmpByteSerializable, IAmpDeserializable
{
    int getJumpPointCount();

    short[] getAllJumpPoints();

    Short getJumpPoint(int _address) throws Exception;

    byte[] getIdentityBytes();

    byte[] serializeToBytes();
}
