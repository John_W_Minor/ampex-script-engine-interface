package engine.data;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

import java.math.BigInteger;

public interface IDataElement extends IAmpByteSerializable, IAmpDeserializable
{
    //region Getters
    byte[] getData();

    Integer getDataAsUnsignedByte();

    Byte getDataAsByte();

    Boolean getDataAsBoolean();

    Short getDataAsShort();

    Character getDataAsChar();

    Long getDataAsUnsignedInt();

    Integer getDataAsInt();

    Float getDataAsFloat();

    Long getDataAsLong();

    Double getDataAsDouble();

    String getDataAsString();

    String getDataAsHexString();

    BigInteger getDataAsBigInteger();

    int getSize();

    byte[] serializeToBytes();
}
