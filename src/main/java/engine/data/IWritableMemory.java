package engine.data;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

public interface IWritableMemory extends IAmpByteSerializable, IAmpDeserializable
{
    //region Getters
    IDataElement getElement(int _address) throws Exception;

    int getElementCount();

    IDataElement[] getAllDataElements();

    //region Setters
    boolean setElement(IDataElement _element, int _address) throws Exception;

    boolean setElement(byte[] _element, int _address) throws Exception;

    IWritableMemory copyWritableMemory();

    byte[] serializeToBytes();
}
