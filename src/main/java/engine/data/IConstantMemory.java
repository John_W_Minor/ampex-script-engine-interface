package engine.data;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

public interface IConstantMemory extends IAmpByteSerializable, IAmpDeserializable
{
    int getElementCount();

    IDataElement[] getAllDataElements();

    IDataElement getElement(int _address) throws Exception;

    byte[] serializeToBytes();
}