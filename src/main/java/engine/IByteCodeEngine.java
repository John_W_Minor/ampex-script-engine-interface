package engine;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.IDataElement;
import engine.data.IWritableMemory;
import engine.operators.IOperator;

import java.util.ArrayList;

public interface IByteCodeEngine
{
    //region Instruction Set
    void addReservedWord8Operators();

    void addOperators(String _package);

    void addOperators(String _package, Class _interface);

    void addOperator(Class iOperatorClass);

    void finalizeOperators();

    IOperator[] getOperators();

    ArrayList<IDataElement> executeProgram(IBinary _binToExecute, IWritableMemory _writableMemory, ITransAPI _transaction, byte[] _executionAddress, boolean _debug) throws Exception;

    int getVersion();

    boolean isWord8();
}
