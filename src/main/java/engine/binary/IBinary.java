package engine.binary;

import amp.Amplet;
import amp.serialization.IAmpAmpletSerializable;
import amp.serialization.IAmpDeserializable;
import com.ampex.amperabase.KeyType;
import engine.data.IConstantMemory;
import engine.data.IJumpMemory;
import engine.program.IProgram;

public interface IBinary extends IAmpAmpletSerializable, IAmpDeserializable
{
    //region Getters
    IProgram getProgram();

    IConstantMemory getConstantMemory();

    IJumpMemory getJumpMemory();

    boolean isWord8();

    int getBCEVersion();

    byte[] getEntropy();

    long getTimestamp();

    byte[] getPublicKey();

    KeyType getPublicKeyType();

    byte[] getFeeAddress();

    long getFeeMultiplier();

    byte[] getProgramIdentityBytes();

    Amplet serializeToAmplet();
}
