package engine.program;

public interface IOPCode
{
    int getBaseCode();

    int getVariantCode();

    char convertToChar();
}
