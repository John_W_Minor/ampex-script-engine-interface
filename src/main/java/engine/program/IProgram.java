package engine.program;

import amp.serialization.IAmpByteSerializable;
import amp.serialization.IAmpDeserializable;

public interface IProgram extends IAmpByteSerializable, IAmpDeserializable
{
    //region Program Counter
    void setProgramCounter(int _index) throws Exception;

    int getProgramCounter();

    void decrementProgramCounter();

    void incrementProgramCounter();

    //region Opcodes
    boolean addOPCode(IOPCode _opCode);

    boolean addOPCode(IOPCode _opCode, boolean _word8);

    IOPCode getNextWord() throws Exception;

    //region Word Mode
    void word8Mode();

    void word16Mode();

    boolean isWord8();

    //region Sealing
    void seal();

    boolean isSealed();

    IProgram copyProgram();

    //region Serialization
    byte[] serializeToBytes();
}
