package engine.operators;

import com.ampex.amperabase.ITransAPI;
import engine.binary.IBinary;
import engine.data.IConstantMemory;
import engine.data.IDataElement;
import engine.data.IJumpMemory;
import engine.data.IWritableMemory;
import engine.program.IOPCode;
import engine.program.IProgram;

import java.util.Stack;

/**
 * Created by Queue on 3/7/2018.
 */
public interface IOperator
{
    String getKeyword();

    int getMaxExecutions();

    int getBaseCode();

    void execute(Stack<IDataElement> _stack, IBinary _binary, IProgram _program, IConstantMemory _constantMemory, IJumpMemory _jumpMemory, IWritableMemory _writableMemory, IOPCode _opCode, ITransAPI _transaction, byte[] _executionAddress) throws Exception;
}
